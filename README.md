# huaweihonorcup

Решение задания заключительного этапа.

## Структура директории:

dataset\test\LR - папка с полученными изображениями

data_preprocessing.ipynb - ноутбук с предобработкой данных

just_reduce_size.ipynb - ноутбук с моделью, уменьшающей изображение классическим способом

simple_mask.ipynb  - ноутбук с финальной моделью

report.pdf - отчет с описанием экспериментов


## Необходимые компоненты:

- ОС: Windows
- Jupyter Notebook
- python 3.7.5
- opencv-python 4.4.0
- torch 1.7.1
- torchvision 0.5.0
- numpy 1.7.13
- pillow 7.0.0
- sklearn 0.21.2
- piq 0.5.2
- torchsummary 1.5.1
- matplotlib 3.1.1
- tqdm 4.42.1
 

## Воспроизведение решения:

Для воспроизведения решения необходимо исполнить ноутбук data_preprocessing.ipynb, а затем simple_mask.ipynb.
